﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {
	private float maxHealth = 100;
	private float currentHealth = 100;
	private float maxArmor = 100;
	private float currentArmor = 100;
	private float maxStamina = 100;
	private float currentStamina = 100;

	public Texture2D healthTexture;
	public Texture2D armourTexture;
	public Texture2D staminaTexture;

	public float walkSpeed = 10.0f;
	public float runSpeed = 10.0f;

	private float canHeal = 0.0f;
	private float canRegenerate = 0.0f;

	private float barWidth;
	private float barHeight;

	private CharacterController chCont;
	private UnityStandardAssets.Characters.FirstPerson.FirstPersonController fpsC;

	private Vector3 lastPosition;

	//Is initialaized before Start()
	void Awake() {
		barHeight = Screen.height * 0.02f;
		barWidth = barHeight * 10.0f;
		chCont = GetComponent<CharacterController> ();
		fpsC = gameObject.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController> ();

		lastPosition = transform.position;

	}

	//This function is using to display User Interface on the screen (like hp, stamina, ammo, current wapon etc.)
	void OnGUI () {
		GUI.DrawTexture (new Rect (Screen.width - barWidth - 10, Screen.height - barHeight - 10, currentHealth * barWidth / maxHealth, barHeight), healthTexture);
		GUI.DrawTexture (new Rect (Screen.width - barWidth - 10, Screen.height - barHeight * 2 - 20, currentArmor * barWidth / maxArmor, barHeight), armourTexture);
		GUI.DrawTexture (new Rect (Screen.width - barWidth - 10, Screen.height - barHeight * 3 - 30, currentStamina * barWidth / maxStamina, barHeight), staminaTexture);


	}
	// This function will calculate our hp and armor after taking hit
	void TakeHit(float damage) {
		if (currentArmor > 0) {
			currentArmor -= damage;
			if (currentArmor < 0) {
				currentHealth += currentArmor;
				currentArmor = 0;
			}
		} else {
			currentHealth -= damage;
		}

		if (currentHealth < maxHealth) {				// Heal possibility check
			canHeal = 5.0f;
		}
			
		currentHealth = Mathf.Clamp (currentHealth, 0, maxHealth); // Set the value betweenn max and min value - it can not be bigger or smaller then min and max (0 and maxHealth in this case)
		currentArmor = Mathf.Clamp (currentArmor, 0, maxArmor);
			
	}

	// Używanie tej funkcji sprawa że używamy fizyki gry a nie FPS-ów. Mamy zapewnione dzięki temu stałe odstępy między wywołaniami.
	void FixedUpdate () {
		float speed = walkSpeed;

		if (chCont.isGrounded && Input.GetKey (KeyCode.LeftShift) && lastPosition != transform.position && currentStamina > 0) {
			lastPosition = transform.position;
			speed = runSpeed;
			currentStamina -= 1;
			currentStamina = Mathf.Clamp (currentStamina, 0, maxStamina);
			canRegenerate = 5.0f;
		}
		if (currentStamina > 0) {
			fpsC.CanRun = true;
		} else {
			fpsC.CanRun = false;
		}

	}

	//Function for regenerate hp and stamina
	void regenerate (ref float currentStat, float maxStat) {
		currentStat += maxStat * 0.005f;
		Mathf.Clamp (currentStat, 0, maxStat);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.P)) {
			TakeHit(30);
		}
		if (canHeal > 0.0f) {
			canHeal -= Time.deltaTime;
		}

		if (canRegenerate > 0.0f) {
			canRegenerate -= Time.deltaTime;
		}
		if (canHeal <= 0.0f && currentHealth < maxHealth) {
			regenerate (ref currentHealth, maxHealth);
		}
		if (canRegenerate <= 0.0f && currentStamina < maxStamina) {
			regenerate (ref currentStamina, maxStamina);
		}
	}
}
